# Burada özel eklentilerimi bulabilirsiniz!

### Olası müşteriler için:

* CS:GO Item, Steam cüzdan kodu veya Steam oyun(fazla ödeme) ile eklenti satın alabilirsiniz.
* Bir eklenti satın alırsanız, güncellemelere ve desteğe erişirsiniz.
* Eklentinin kaynak koduna erişiminiz olacak.
* Bu desteğe ve güncellemelere erişiminiz olması için bir gitlab.com hesabına sahip olmanız gerekir, hesap açmak ÜCRETSİZDİR.


### Bayiliklere izin verilmez.

* Eklentiyi alıp bir başkasıyla paylaşmak / satmak yasaktır. Eklenti yalnızca sizin topluluk sunucunuz içindir.
* FTP erişimine sahip kişilerin eklentiyi indirip yeniden satmasını önlemek için kaynak kodunu (.sp dosyası) sunucunuza yüklemeyin.
* **:zap::zap::zap: Eğer birisinin eklentileri alıp sattığını veya ücretsiz olarak birine dağıttığını bana video ve görsellerle kanıtlarsanız eklentiyi herkese açık konuma getiririm. :zap::zap::zap:**


### Özel eklenti listesi:

* **Sergilediğim siteye git**: http://sourceturk.net/eklentiler.html


* **Deagle Sustum**: Herşey otomatik olarak çalışır. Ekranın ortasında bir yazı belirir yazıyı ilk yazan oyuncuya 1 mermili deagle verilir. Eğer oyuncu bu deagle ile birini vurabilirse oyun devam eder. Ama birini vuramazsa oyuncunun elindeki deagle silinir ve otomatik öldürülür.
* **MadMax**: Oyun başlamadan önce geri sayımlıdır, ilk 30 saniye silah kullanılamaz, ilk 30 saniye ölenler otomatik yeniden canlandırılır. Renkli mesajlar, hatasız fixlenmiş harita. Araba sürüşlerindeki hatalar yok! Ve daha bir çok modül.	
* **Daire**: !daire komutuyla mahkum takımını baktığınız yere daire şeklinde dondurur ve ışınlar.
* **Komutçu Otomatik Respawn Sistemi**: !w yazan kişi öldüğünde otomatik canlandırılır. 3 hakkı vardır. 10 saniye geri sayımlı olarak yeniden doğdurulur. Doğma ve hak süreleri ayarlanabilir , renkli mesajlara sahiptir.				
* **Renkli Chat Düzeni**: Sourcemod'un kendi beyaz ve renksiz mesajlarını pasif konuma getirir, ve her komutunuzda rengarenk mesajlar çıkar.
* **Hook Engeli**: Round başladığında ilk 15 saniye hook otomatik kapalı olur, kaçak kapısına hookla giden CT-T engellemek içindir. 15 saniye sonra otomatik açılır.
* **Sayı Oyunu**: !sayioyunu komutu yazıldığında eklenti 1-50 arasında eklenti otomatik bir sayı belirler ve oyunculara oyunun başladığını haber verir. Belirlenen sayıyı ilk yazana kredi verilir, oyun sona erer.						
* **Özel FF - Freeze Menu**: Renkli mesajlar, seçilebilen bir çok saniye. FF Menü'de mahkum takımına silah-tabanca menüsü. FF açıldığında otomatik bunny-otorev kapatılır. Freeze menu ve FF menu eklentileri oldukça eğlenceli!
* **Otomatik İsyan Kredisi**: JailBreak haritası oynanıyor ise ve mahkum takımı kazandıysa isyan kredisi alır, gardiyan takımı kazandıysa isyan olmadığı için kredi alır. Krediler ayarlanabilir.		
* **Wallhack**: Adminlere özel, oyuncular parlar.
* **Grup Tagı Kredisi**: Grup tagınızı kullanan oyuncular belli bir saniye başına belli bir kredi alırlar. Grup tagı/saniye/kredi ayarlanabilir.						
* **Grup Tagı Parası**: Grup tagınızı kullanan oyuncular her round ekstra para alırlar. Grup tagı/para ayarlanabilir.						
* **Grup Tagı Kısıtlama**: Sunucunuzda başka sunucuların/grupların taglarını kullanmayı tamamen kısıtlayan bir eklentidir.						
* **Çekiliş**: !cek yazdığınızda rastgele bir oyuncu çıkar.						
* **Renkli TS3 ve IP Gösterme**: !ip - !ts3 vb. komutları içerir. Renkli ve göze hoş gelir! IP adreslerinizi duyurabilirsiniz.						
* **Zamanlı FF**: !ffzaman <saniye> komutunu uyguladığınızda CSAY'den geri sayımlı renkli ff sayacıdır. Son 10 saniye kala otorev kapatılır, canlar yenilenir.						
* **Zamanlı Freeze**: !fz <saniye> komutunu uyguladığınızda CSAY'den geri sayımlı renkli freeze sayacıdır. Son 10 saniye kala otorev kapatılır, canlar yenilenir.						
* **RedBull**: CS 1.6'daki keyifli isyanlar geri döndü. !redbull komutu ile 15 saniyelik hız ve yarı yer çekimi kazanırsınız. İstenirse bu komut kredi karşılığı ayarlanabilir.						
* **Sayılı ve Renkli İzleyici Listesi**: Klasik isim gösteren izleyici listelerine son. Artık sizi kaç kişi izlediğini renkli bir biçimde göreceksiniz. Sizi izleyen kişiyede isminiz ve kaç kişi tarafından sizin izlendiğiniz yazar. Bunu istemeyen oyuncular !izleyici veya !izleme komutuyla izleyici listesini kapatabilirler.
* **Jackpot**: Hatasız, renkli jackpot eklentisidir. !jackpot yazıldığında menü açılır jackpot hakkında oyuncuya bilgi sunulur. Klasiktir, ama bir çok hata vb. düzeltilmiştir. Yenilikler getirilip, renkli ve menülü hale sunulmuştur.
* **Oyuncuya Hoşgeldin Mesajları**: Oyuncu oyuna girdiğinde CSAY'dan renkli olarak Hoşgeldiniz, iyi oyunlar mesajları geçer.
* **Anti-Rush Sistemi**: Aim Redline haritası için otomatik ayarlı biçimde sağlanır. Engeller kaldırıldığında CSAY ve normal sohbetten renkli olarak mesajlar geçer. 1vs1 kaldığında otomatik engeller kaldırılır. Engellerin kaldırma süresi ayarlanabilir. Herşey Türkçe'dir. Hatalar düzeltilmiştir.
* **NoScope Dedektörü**: Eğer oyuncu birini noscope vurursa (AWP - SSG08 vb.) sohbetten oyuncunun adı ve noscope vurduğu duyurusu geçer. Aynı zamanda noscope ve kafadan vuruş yaparsa yine sohbetten duyuru geçer. İstenirse vuran kişiye kredi verme özelliği aktif edilebilir, istenirse edilmez.
* **Uyarı**: Bir yetkili bir oyuncuyu veya bir yetkiliyi belli bir sebepten dolayı uyarabilir. Uyarılar dosyaya düşer oradan uyarıları görebilirsiniz.
* **Renkli Kısa Komutlar**: 80'e yakın kısa komut, ve komutlar uygulandığında sohbetten geçen renkli mesajlar.
* **Şikayet**: Bir oyuncu belli bir sebepten veya belli bir oyuncudan dolayı şikayet bırakabilir. Şikayetler dosyaya düşer oradan gelen şikayetleri görebilirsiniz.
* **1 Kişiye CVAR**: Örneğin tek bir kişiye sv_cheats 1 yapabilirsiniz. Ve o yaptığınız kişiden başkaları bundan yararlanamaz.
* **Piyango**: Oyuncu piyango komutunu kullandığında kasa açma sesi gelir ve ekranın ortasında sürekli kredi miktarı değişir. En son kasa açma sesi biter ve bir kredide durur. Kazandığı miktarı alır. Giriş ücreti / minimum ödül / maximum ödül ayarlanabilir.
* **Hazır CHATCOLORS - ADMINSIMPLE.INI**: Hazır chatcolors, admin dosyaları herşeyi hazırdır. Yaklaşık 15'e yakın yetki vardır. İçinde slotlar - komutçular vs. herşey mevcuttur.
* **Takım**: !takim komutu aktiftir. Ayrıca takım komutunun en iyi yani @ct - @t - @all ve isim ile çalışır. Ve aynı zamanda karşı takıma atılan kişinin adı renkli bir duyuru ile sohbetten geçer.
* **Kredili Tavuk**: !kt komutuyla yetkililer tavuk oluşturur. Oluşturulan tavuktan rastgele krediler çıkar. Sohbeti renklidir. Minimum / Maximum çıkacak ödüller ayarlanabilir.
* **Yetkililere Özel Para**: Yetkililere her round başında ekstra para verilir. Para ayarlanabilir.
* **Kredi Log**: Sunucunuzda !hediye veya !krediver komutu uygulandığında bir dosyaya şu şekilde geçer ; "Kredi veren / hediye eden kişi, edilen kişi, miktar, saat, tarih"
* **Round Sonu Müzikleri**: 47 adet son günlerde çıkan bir çok popüler şarkıyı içerir. 10 saniyelik ve hepsi ayarlıdır.
* **Gelişmiş Küfür Engelleme**: Bir oyuncu küfür ederse öncelikle oyuncu susturulur ve sohbetten renkli bir yazı olarak ; "Vortéx isimli oyuncu küfür ettiği için 1 dakika susturuldu." İsim temsilidir. 1 dakika sonra oyuncunun susturulması kalkar. Bu arada küfür ettiğinde sadece küfür eden oyuncuya ettiği küfür sohbetten geçer.
* **LOTO**: Yeni eklenen bu eklentimiz !loto X X X X X X şeklinde çalışmaktadır. X'leri birer sayı olarak düşünün. Oyuncu bu komutu uygular. Ve eklentide rastgele 1 ile 10 arasında sayılar belirler. 1 sayı bilirse, 2 sayı bilirse ... 6'ya kadar gider. Bilme oranına göre kredi alır.
* **"sm plugins" Komutlarını Engelleme**: SM komutlarının hepsi engellenebilir. Yazıldığında çıkan yazı ayarlanabilir!
* **Lazerle Havaya Yazı Yaz**: !write - !lazeryaz - !yaz komutlarıyla yazdığınız cümleleri lazerle havaya yazar. Örnek: http://i.hizliresim.com/41ORmL.jpg
* **Slot Ol**: !slotol komutuyla oyuncu kendini slot yapabilir. Slot olan oyuncuya bir menü açılır toplantılar, bilgiler, ip adresleri hakkında bilgi verilir. Yetkisi olan oyuncular bu komutu kullanamaz.
* **Yetkili Giriş Mesajları**: Her yetkili için ayrı ayrı CSAY mesajı olur. Örnek; Komutçu Vortéx! Şuanda oyunda.
* **Mermi Uyarısı**: Merminiz azaldığında sesli ve CSAY'dan bir uyarı gelir. Ve CSAY uyarısı renkli bir şekilde şöyledir; AZ MERMİ! 4/30. Buradaki sayılar temsilidir.
* **FreeKill Uyarısı**: Bir mahkum öldüğünde !freekill komutunu uygular. CT takımına kişinin adı ve Free olup olmadığı ile ilgili bir menü açılır. CT takımının seçimine göre oyuncu yeniden canlandırılır.
* **Komutçu Icon**: !w yazıldığında komutçunun kafasına .PNG bile koyabilirsiniz. İstediğiniz resimi/görseli/logoyu koyabilirsiniz.
* **İsyan Sesi**: Bir mahkum, gardiyana İLK vurduğu zaman isyan sesi çalar, sohbetten ve csay'dan mahkumun adı ve isyan başlattığı duyurusu geçer. Ses değiştirilebilir.				    
* **Komutçu Admini**: !ka ile yalnızca bir yetkili komutçu admini olur. Komutçu admini olduğunda sohbetten ve csay'den duyuru geçilir. !kk ile komutçu admininden ayrılabilir.


# Steam üzerinden benimle iletişime geçebilirsiniz. http://steamcommunity.com/id/emreires42/